# Java - Serenity - Cucumber - Junit - Maven

## Getting Started

In the process of refactoring, I added the CommonStepDefinitions class, where I exported common methods. In it, I redesigned the code status check, made it more universal, and added a JSON structure check. Changed the method names to be more readable, clearer and in the same style.
in the SearchStepDefinitions class, I left only the method that refers to this set of tests. It implemented the sending of a GET request.
Added the serenity-single-page-report dependency to generate the report.

## Built With

* [Maven](https://maven.apache.org/) - Build tool
* [Junit](https://junit.org/junit5/) - Java testing framework
* [Cucumber](https://cucumber.io) - BDD testing tool

## Running the tests

### 1. clean, build and run tests
```console
$ mvn clean test
```
### 2. generate report
```console
$ mvn serenity:reports -Dserenity.reports=single-page-html
```