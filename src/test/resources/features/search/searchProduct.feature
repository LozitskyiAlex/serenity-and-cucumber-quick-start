Feature: Search for the product

### Please use endpoint GET https://waarkoop-server.herokuapp.com/api/v1/search/demo/{product} for getting the products.
### Available products: "orange", "apple", "pasta", "cola"
### Prepare Positive and negative scenarios

  @Positive
  Scenario Outline: GET /search/demo/{product} Search for the <product> positive 200
    Given I get products by search "search/demo/<product>"
    Then I check that status code is 200
    And I check that the response body contains structure:
      | provider | title | url | price | unit | isPromo | promoDetails | image |
    Examples:
      | product |
      | orange  |
      | apple   |
      | pasta   |
      | cola    |

  @Negative
  Scenario: GET /search/demo/{product} Search for the non-existent product negative 404
    Given I get products by search "search/demo/non-existent"
    Then I check that status code is 404
    And I check that the response body contains structure:
      | detail | error | message | requested_item | served_by |
    And I check that there is error in results
