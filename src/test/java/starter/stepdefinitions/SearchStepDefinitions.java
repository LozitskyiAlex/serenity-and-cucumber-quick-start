package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import net.thucydides.core.ThucydidesSystemProperty;
import net.thucydides.core.util.EnvironmentVariables;

import static net.serenitybdd.rest.SerenityRest.given;

public class SearchStepDefinitions {
    EnvironmentVariables environmentVariables;

    @Given("I get products by search {string}")
    public void getProductBySearch(String uri) {
        String baseUrl = ThucydidesSystemProperty.WEBDRIVER_BASE_URL.from(environmentVariables);
        given().when().get(baseUrl + uri);
    }
}
