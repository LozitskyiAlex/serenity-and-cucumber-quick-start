package starter.stepdefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.Assert;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static net.serenitybdd.rest.SerenityRest.then;
import static org.hamcrest.Matchers.*;

public class CommonStepDefinitions {

    @Then("I check that status code is {int}")
    public void checkStatusCode(int statusCode) {
        restAssuredThat(response -> response.statusCode(statusCode));
    }

    @Then("I check that there is error in results")
    public void checkThatThereIsError() {
        restAssuredThat(response -> response.body("detail.error", equalTo(true)));
    }

    @And("I check that response {string} contains {string}")
    public void checkThatResponseContains(String key, String val) {
        restAssuredThat(response -> response.body(key, contains(val)));
    }

    @Then("^I check that the response body contains structure:$")
    public void verifyJSONResponseStructure(DataTable dataTable) {
        dataTable.asList().forEach(item ->
            Assert.assertTrue("The response does not contain the item: " + item,
                    then().extract().response().asString().contains(item)));
    }
}
